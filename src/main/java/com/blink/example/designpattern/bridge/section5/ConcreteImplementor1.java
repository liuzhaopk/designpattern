package com.blink.example.designpattern.bridge.section5;

/**
 * @author cbf4Life cbf4life@126.com
 * I'm glad to share my knowledge with you all.
 */
public class ConcreteImplementor1 implements Implementor{

	@Override
    public void doSomething(){
		//业务逻辑处理
	}
	
	@Override
    public void doAnything(){
		//业务逻辑处理
	}
}