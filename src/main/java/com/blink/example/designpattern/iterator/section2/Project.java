package com.blink.example.designpattern.iterator.section2;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

import java.util.ArrayList;
import java.util.List;

/**
 * <Description> </Description>
 * <ClassName> Project</ClassName>
 *
 * @author liuxianzhao
 * @date 2018年03月23日 19:01
 */
public class Project implements IProject {
    List<IProject> projectList = new ArrayList<>();
    private String name;
    private Integer num;
    private Integer cost;

    @Override
    public void add(String name, int num, int cost) {
        projectList.add(new Project(name, num, cost));
    }

    @Override
    public String getProjectInfo() {
        return null;
    }

    @Override
    public IProjectIterator iterator() {
        return null;
    }

    public Project(String name, Integer num, Integer cost) {
        this.name = name;
        this.num = num;
        this.cost = cost;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }

    public Project() {
    }
}
