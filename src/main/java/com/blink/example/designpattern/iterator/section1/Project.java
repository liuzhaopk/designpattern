package com.blink.example.designpattern.iterator.section1;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

/**
 * <Description> </Description>
 * <ClassName> Project</ClassName>
 *
 * @author liuxianzhao
 * @date 2018年03月23日 19:01
 */
public class Project implements IProject {
    private String name;
    private Integer renshu;
    private Integer codeline;

    @Override
    public String getProjectInfo() {
        return null;
    }

    public Project(String name, Integer renshu, Integer codeline) {
        this.name = name;
        this.renshu = renshu;
        this.codeline = codeline;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }
}
