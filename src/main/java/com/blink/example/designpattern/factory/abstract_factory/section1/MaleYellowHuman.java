package com.blink.example.designpattern.factory.abstract_factory.section1;

/**
 * @author cbf4Life cbf4life@126.com
 * I'm glad to share my knowledge with you all.
 */
public class MaleYellowHuman extends AbstractYellowHuman {

	//黄人男性
	@Override
    public void getSex() {
		System.out.println("黄人男性");
	}

}