package com.blink.example.designpattern.strategy.section2.impl;

import com.blink.example.designpattern.strategy.section2.Strategy;

/**
 * @author cbf4Life cbf4life@126.com
 * I'm glad to share my knowledge with you all.
 */
public class ConcreteStrategy2 implements Strategy {

	@Override
    public void doSomething() {
		System.out.println("具体策略2的运算法则");
	}

}