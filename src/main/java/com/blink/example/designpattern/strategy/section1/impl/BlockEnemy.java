/**
 * 
 */
package com.blink.example.designpattern.strategy.section1.impl;

import com.blink.example.designpattern.strategy.section1.IStrategy;

/**
 * @author cbf4Life cbf4life@126.com
 * I'm glad to share my knowledge with you all.
 * 孙夫人断后，挡住追兵
 */
public class BlockEnemy implements IStrategy {

	@Override
	public void operate() {
		System.out.println("孙夫人断后，挡住追兵");
	}

}