package com.blink.example.designpattern.proxy.cglib;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

public class CglibDynamicProxy {

    Object originalObj;

    public Object bind(Object originalObj) {
        this.originalObj = originalObj;
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(originalObj.getClass());
        enhancer.setCallback(new MethodInterceptor() {
            @Override
            public Object intercept(Object obj, Method method, Object[] arg, MethodProxy proxy) throws Throwable {
                System.out.println("welcome");
                Object object = proxy.invokeSuper(obj, arg);
                return object;
            }
        });
        return enhancer.create();
    }


}