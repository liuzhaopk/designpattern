package com.blink.example.designpattern.proxy.jdk;

public class Hello implements IHello {

    @Override
    public void sayHello() {
        System.out.println("hello world");
    }
}