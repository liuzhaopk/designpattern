package com.blink.example.designpattern.proxy;

import com.blink.example.designpattern.proxy.cglib.CglibDynamicProxy;
import com.blink.example.designpattern.proxy.cglib.Hello;
import net.sf.cglib.core.DebuggingClassWriter;

/**
 * <ClassName>JdkDynamicProxyTest</ClassName>
 * <Description>JDK动态代理</Description>
 *
 * @author liuxianzhao
 * @date 2018/3/17 21:01
 */
public class CglibDynamicProxyTest {
    public static void main(String[] args) {
        System.setProperty(DebuggingClassWriter.DEBUG_LOCATION_PROPERTY, "E:\\gitee\\designpattern\\com\\sun\\proxy");
        Hello hello = (Hello) new CglibDynamicProxy().bind(new Hello());
        hello.sayHello();
    }
}