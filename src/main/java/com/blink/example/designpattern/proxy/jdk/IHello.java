package com.blink.example.designpattern.proxy.jdk;

public interface IHello {
    void sayHello();
}