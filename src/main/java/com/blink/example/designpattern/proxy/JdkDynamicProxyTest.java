package com.blink.example.designpattern.proxy;

import com.blink.example.designpattern.proxy.jdk.DynamicProxy;
import com.blink.example.designpattern.proxy.jdk.Hello;
import com.blink.example.designpattern.proxy.jdk.IHello;

/**
 * <ClassName>JdkDynamicProxyTest</ClassName>
 * <Description>JDK动态代理</Description>
 *
 * @author liuxianzhao
 * @date 2018/3/17 21:01
 */
public class JdkDynamicProxyTest {
    public static void main(String[] args) {
        System.setProperty("sun.misc.ProxyGenerator.saveGeneratedFiles", "true");
        IHello hello = (IHello) new DynamicProxy().bind(new Hello());
        hello.sayHello();
    }
}