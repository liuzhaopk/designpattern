package com.blink.example.designpattern.six_rule.dependency_inverse.section3;

/**
 * @author cbf4Life cbf4life@126.com
 * I'm glad to share my knowledge with you all.
 * 司机
 */
public class Driver implements IDriver{
	
	//司机的主要职责就是驾驶汽车
	@Override
    public void drive(ICar car){
		car.run();
	}
}