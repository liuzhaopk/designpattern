package com.blink.example.designpattern.six_rule.dependency_inverse.section2;

/**
 * @author cbf4Life cbf4life@126.com
 * I'm glad to share my knowledge with you all.
 * 奔驰汽车
 */
public class Benz implements ICar{

	//汽车肯定会跑
	@Override
    public void run(){
		System.out.println("奔驰汽车开始运行...");
	}
}