package com.blink.example.designpattern.six_rule.lishi_replace.section1;

/**
 * @author cbf4Life cbf4life@126.com
 * I'm glad to share my knowledge with you all.
 * 机枪
 */
public class MachineGun extends AbstractGun{
	
	@Override
    public void shoot(){
		System.out.println("机枪扫射...");
	}
}