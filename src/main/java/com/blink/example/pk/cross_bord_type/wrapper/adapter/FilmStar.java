package com.blink.example.pk.cross_bord_type.wrapper.adapter;

/**
 * @author cbf4Life cbf4life@126.com
 * I'm glad to share my knowledge with you all.
 * 电影明星
 *  
 */
public class FilmStar implements IStar {

	@Override
    public void act(String context) {
		System.out.println("明星演戏：" + context);
	}

}