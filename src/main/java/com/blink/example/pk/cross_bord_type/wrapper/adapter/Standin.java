package com.blink.example.pk.cross_bord_type.wrapper.adapter;

/**
 * @author cbf4Life cbf4life@126.com
 * I'm glad to share my knowledge with you all.
 */
public class Standin implements IStar {
	private IActor actor;
	
	//替身是谁
	public Standin(IActor _actor){
		this.actor = _actor;
	}
	
	@Override
    public void act(String context) {
		actor.playact(context);
	}

}